Installation Notes
******************

ssh-deploy-key is normally installed via pip.  However, on some systems,
there are source files that must be installed first.


Install the Prerequisites
=========================

ssh-deploy-key depends on the Paramiko ssh library, which
requires the Python sources.  You can install these
using the normal package managers for your OS.


Debian/Ubuntu (apt-get)
-----------------------
::

    sudo apt-get install python-dev


Red Hat/Centos (yum)
-------------------
::

    sudo yum install python-devel

OS X
----

On macs, the Python sources do not need to be installed, but a compiler
is required. And compilers are not pre-installed by default.

Normally, you can just run the pip command to install ssh-deploy-key
(see below), and if no compiler is currently on your system, you will
be prompted to install one:

.. image:: images/install_gcc_mac.png

If this happens, click the 'install' button, then run the pip
command again.


Install ssh-deploy-key via Pip
==============================
Once the development stuff is in place,  ssh-deploy-key is
installed with the pip command.  Pip is the standard Python
package installation tool.  To get it, see
http://www.pip-installer.org/en/latest/installing.html

Then,
::

    sudo pip install ssh-deploy-key


Boom!  Done!