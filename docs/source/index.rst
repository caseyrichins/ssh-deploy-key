SSH Deploy Key
*******************


Overview
========

SSH deploy-key is an easy-to-use tool that deploys ssh
keys to remote hosts.  It makes deploying to a single host
simple. It makes deploying to many hosts simple and fast.


Features
========

 * ssh-deploy-key is a simple tool, that focuses on doing one task very well.
 * It can be run interactively, or in batch mode for scripted deployments.
 * A variety of input options are supported via shell redirection.
 * Deploys to multiple remote hosts are done in parallel, for maximum performance.


Source Code
===========

The source code for ssh-deploy-key code is hosted
on Bitbucket at this location:

https://bitbucket.org/travis_bear/ssh-deploy-key


Contents
========

.. toctree::
   :maxdepth: 1
   
   Compatibility <compatibility>
   Installation <installing>
   Usage <using>
   License <license>
   Alternatives <alternatives>