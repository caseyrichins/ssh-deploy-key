Alternatives
************

There are lots of ways to copy out an ssh key to a
remote host.  It can be done by hand.  It can be
done with existing tools like ssh-copy-id.  It can
be done using configuration management tools. So why
use SSH Deploy Key?

Although ssh-deploy-key is not ideal for every scenario, its
speed and simplicity make it a good choice in many situations.
It is a tool that is tightly focused on one task only -- moving ssh
keys out to remote hosts as easily and as quickly as possible.

Here is a comparison of ssh-deploy-key with some other common
ways to deploy a key.


Manual Deployment
-----------------
Deploying ssh keys by hand is a time-honored technique that
in general works pretty well. However, in almost all cases,
using ssh-deploy-key is a better option.  It's faster, easier,
more reliable, and more repeatable.  When deploying to
more than one host at a time, these advantages only multiply
with ssh-deploy-key's bulk deployment abilities.

There is one use case where deploying by hand is a better bet:
when the remote host is on a different network, behind a jump box.
ssh-deploy-key does not handle that scenario.


ssh-copy-id
-----------

ssh-copy-id is a great tool, but it's not the ideal
solution for every scenario.

 * ssh-copy-id is not installed by default on all systems,
   notably on Mac OS.

 * ssh-copy-id has no concept of 'smart append'.  It
   will append a key to a remote host's authorized keys file
   regardless of whether that key is already present.

 * Scripting the use of ssh-copy-id for deploying to
   multiple remote hosts can be challenging:

     * The password must be entered interactively for each host.
     * In the case where there are remote hosts that have not
       been seen before, users must interactively
       allow each host to be added to the known_hosts file.


Configuration Management Tools
------------------------------

Configuration management tools (like Puppet, Ansible,
etc.) can do a terrific job deploying ssh key(s).
But if you are not already set up to use them for key
distribution, these general-purpose solutions can be
overkill, especially when compared with a dedicated tool
like ssh-deploy-key that is focused on doing a single
task.

