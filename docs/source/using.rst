Overview
********

Although ssh-deploy-key supports several options, it has sane defaults.  In the
simplest case, just pass it the name of the remote host you want to deploy to.
::

    ssh-deploy-key <hostname>


Usage
*****

ssh-deploy-key [ options ] [ remote host[s] ]

Options
=======
::

    usage: ssh-deploy-key [-h] [-a AUTHORIZED_KEYS] [-d] [-k KEY_FILE]
                      [-m TIMEOUT_SECONDS] [-o PORT] [-p PASSWORD]
                      [-s SSH_DIR] [-t THREADS] [-u USERNAME]
                      [hosts [hosts ...]]

    Distribute an ssh key to remote hosts.

    positional arguments:
      hosts                 Zero or more remote hosts to receive the ssh key. If
                            this value is unspecified, remote hosts will be read
                            from standard in.

    optional arguments:
    -h, --help              show this help message and exit
    -a AUTHORIZED_KEYS, --authorized_keys AUTHORIZED_KEYS
                            Name of the remote authorized keys file. (Changing
                            this setting is uncommon.)
    -d, --append            Add the ssh key to the end of the remote authorized
                            keys file instead of overwriting it. Default is
                            false.  Key will only be added if it's not already
                            present.
    -k KEY_FILE, --key_file KEY_FILE
                            Path to the local public ssh key file. Default is
                            ~/.ssh/id_rsa.pub
    -m TIMEOUT_SECONDS, --timeout_seconds TIMEOUT_SECONDS
                            Timeout value (in seconds) for connecting to each
                            host. Default is 3
    -o PORT, --port PORT    The ssh port to connect to the remote hosts on.
                            Default is 22
    -p PASSWORD, --password PASSWORD
                            Password to use on remote hosts. If not specified
                            here, you will be prompted for this interactively.
    -s SSH_DIR, --ssh_dir SSH_DIR
                            Directory to copy the key into on the remote host.
                            Default is ~/.ssh
    -t THREADS, --threads THREADS
                            Number of threads to use for simultaneous key
                            distribution. Default is 100.
    -u USERNAME, --username USERNAME
                            Username to use on remote hosts. Default is <current user>

Remote hosts can be given in either the 'hostname' or 'user@hostname'
format.  If 'user@hostname' format is used, this value for 'user' will
override anything specified in the -u/--username arguments.


Examples
========
These are some of the common ways to use ssh-deploy-key

Specifying remote hosts interactively
-------------------------------------
ssh-deploy-key can run interactively.  The user will be prompted for additional
hosts until typing 'exit' or ^D.
::

    [~/git/ssh-deploy-key/bin]$ ssh-deploy-key
    Enter common password for remote hosts:
    Distributing key '/Users/travis/.ssh/id_rsa.pub' to remote hosts in overwrite mode.
    Enter one hostname per line.  Terminate with 'exit' or ^D.
    192.168.1.112
      copying key to travis@192.168.1.112:~/.ssh/authorized_keys... [SUCCESS!]
    192.168.1.113
      copying key to travis@192.168.1.113:~/.ssh/authorized_keys... [SUCCESS!]
    exit

Note that if you do not specify a password for the remote
host on the command line, you will prompted for it
interactively.

Specifying remote hosts on the command line
-------------------------------------------
::

    [~/git/ssh-deploy-key/bin]$ ssh-deploy-key 192.168.1.112 othello@192.168.1.101
    Enter common password for remote hosts:
    Distributing key '/Users/travis/.ssh/id_rsa.pub' to remote hosts in overwrite mode.
      copying key to travis@192.168.1.112:~/.ssh/authorized_keys... [SUCCESS!]
      copying key to othello@192.168.1.101:~/.ssh/authorized_keys... [SUCCESS!]


Note the mix of 'host' and 'user@host' formats for the remote host.


With Shell Redirection
----------------------
ssh-deploy-key accepts piped input.  For example, if you had a script
to generate a list of hosts, you could run it this way
::

    get_host_list.sh | ssh-deploy-key

From a data File
----------------
If you have a data file listing your hosts already, you can redirect
standard in from the file
::

    ssh-deploy-key < host_list


Specifying the username and password on the command line
--------------------------------------------------------
::

    ssh-deploy-key -u root -p p@ssw0rd host1
